﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class GroupReport
    {
        public int Id { get; set; }
        public ApplicationUser Creator { get; set; }
    }
}
