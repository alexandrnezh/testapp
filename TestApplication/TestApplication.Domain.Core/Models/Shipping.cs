﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class Shipping
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int DepartureCityId { get; set; }
        public int ArrivalCityId { get; set; }
    }
}
