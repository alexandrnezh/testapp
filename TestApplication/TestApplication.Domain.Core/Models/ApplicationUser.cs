﻿using Microsoft.AspNetCore.Identity;
using System;

namespace TestApplication.Domain.Core.Models
{
    public class ApplicationUser : IdentityUser
    {
        public DateTime DateCreated { get; set; }
        public string IconImage { get; set; }
    }
}
