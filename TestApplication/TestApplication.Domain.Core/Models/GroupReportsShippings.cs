﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class GroupReportsShippings
    {
        public int Id { get; set; }
        public GroupReport GroupReport { get; set; }
        public Shipping Shipping { get; set; }
    }
}
