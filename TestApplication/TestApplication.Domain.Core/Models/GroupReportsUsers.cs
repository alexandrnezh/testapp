﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class GroupReportsUsers
    {
        public int Id { get; set; }
        public GroupReport Report { get; set; }
        public ApplicationUser User { get; set; }
    }
}
