﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class ReportsUsers
    {
        public int Id { get; set; }
        public Report Report { get; set; }
        public ApplicationUser User { get; set; }
    }
}
