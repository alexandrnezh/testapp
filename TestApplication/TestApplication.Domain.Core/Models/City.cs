﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
