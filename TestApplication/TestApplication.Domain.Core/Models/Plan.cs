﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
    public class Plan
    {
        public int Id { get; set; }
        public int PlannedAmount { get; set; }
        public int Month { get; set; }
        public City DepartureCity { get; set; }
        public City ArrivalCity { get; set; }
    }
}
