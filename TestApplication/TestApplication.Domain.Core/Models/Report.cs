﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Core.Models
{
     public class Report
    {
        public int Id { get; set; }
        public Plan Plan { get; set; }
        public int FactAmount { get; set; }
        public ApplicationUser Creator { get; set; }
    }
}
