﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Business.Sorting
{
    public class ShippingSortingByDate : ISorting<Shipping>
    {
        public List<Shipping> Sort(List<Shipping> items)
        {
            return items.OrderBy(i => i.Date.Day).ToList();
        }
    }
}
