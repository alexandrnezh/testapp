﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class GroupReportsUsersStore : IGroupReportsUsersStore
    {
        private GroupReportsUsersRepository repository;
        public GroupReportsUsersStore()
        {
            repository = new GroupReportsUsersRepository();
        }

        public async Task<List<GroupReport>> GetReportsByUserIdAsync(string id)
        { 
            List<GroupReport> groupReports = new List<GroupReport>();
            var groupReportsUsers = await repository.GetItemsAsync();
            foreach (var item in groupReportsUsers.Where(gr => gr.User.Id == id).ToList())
            {
                groupReports.Add(item.Report);
            }
            return groupReports;
        }
    }
}
