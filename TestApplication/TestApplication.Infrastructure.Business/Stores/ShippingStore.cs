﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Business.Sorting;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class ShippingStore : IShippingStore
    {
        private ShippingRepository repository;
        private CityRepository cityRepository;
        private ISorting<Shipping> sorting;
        public ShippingStore()
        {
            repository = new ShippingRepository();
            cityRepository = new CityRepository();
            this.sorting = new ShippingSortingByDate();
        }
        public void SetSorting(ISorting<Shipping> sorting)
        {
            this.sorting = sorting;
        }
        public async Task<Shipping> GetShippingAsync(int id)
        {
            return await repository.GetItemAsync(id);
        }

        public async Task<List<Shipping>> GetShippingsAsync()
        {
            List<Shipping> shippings = await repository.GetItemsAsync();
            shippings = this.sorting.Sort(shippings);
            return shippings;
        }

        public async Task<List<Shipping>> GetShippingsByMonthCitiesAsync(int month, int depCityId, int arrCityId)
        {
            List<Shipping> shippings = await repository.GetItemsAsync();
            shippings = shippings.Where(s => s.Date.Month == month && s.DepartureCityId==depCityId && s.ArrivalCityId == arrCityId).ToList();
            shippings = this.sorting.Sort(shippings);
            return shippings;
        }

        public async Task<List<Shipping>> GetShippingsByMonthDepartureCityAsync(int month, int depCityId)
        {
            List<Shipping> shippings = await repository.GetItemsAsync();
            shippings = shippings.Where(s => s.Date.Month == month && s.DepartureCityId == depCityId).ToList();
            shippings = this.sorting.Sort(shippings);
            return shippings;
        }
    }
}
