﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class PlanStore : IPlanStore
    {
        private PlanRepository repository;
        private CityRepository cityRepository;
        public PlanStore()
        {
            repository = new PlanRepository();
            cityRepository = new CityRepository();
        }
        public async Task<Plan> GetPlanAsync(int id)
        {
            return await repository.GetItemAsync(id);
        }

        public async Task<Plan> GetPlanByMonthCitiesAsync(int month, int depCityId, int arrCityId)
        {      
            var plans = await GetPlansAsync();
            return plans.FirstOrDefault(p => p.Month == month && p.DepartureCity.Id==depCityId && p.ArrivalCity.Id == arrCityId);
        }

        public async Task<List<Plan>> GetPlansByMonthDepartureCityAsync(int month, int depCityId)
        {
            var plans = await GetPlansAsync();
            return plans.Where(p => p.Month == month && p.DepartureCity.Id == depCityId).ToList();
        }

        public async Task<List<Plan>> GetPlansAsync()
        {
            return await repository.GetItemsAsync();
        }
    }
}
