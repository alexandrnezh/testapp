﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class ReportsShippingsStore : IReportShippingsStore
    {
        private ReportsShippingsRepository repository;
        public ReportsShippingsStore()
        {
            repository = new ReportsShippingsRepository();
        }

        public async Task<List<Shipping>> GetShippingsByReportIdAsync(int id)
        {
            var shippings = new List<Shipping>();
            var reportsShippings = await repository.GetItemsAsync();
            foreach (var item in reportsShippings.Where(gr => gr.Report.Id == id).ToList())
            {
                shippings.Add(item.Shipping);
            }
            return shippings;
        }
    }
}
