﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class ReportStore : IReportStore
    {
        private ReportRepository repository;
        public ReportStore()
        {
            repository = new ReportRepository();
        }
        public async Task<Report> GetReportAsync(int id)
        {
            return await repository.GetItemAsync(id);
        }

        public async Task<List<Report>> GetReportsAsync()
        {
            return await repository.GetItemsAsync();
        }
    }
}
