﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class GroupReportsShippingsStore : IGroupReportsShippingsStore
    {
        private GroupReportsShippingsRepository repository;
        public GroupReportsShippingsStore()
        {
            repository = new GroupReportsShippingsRepository();
        }

        public async Task<List<Shipping>> GetShippingsByReportIdAsync(int id)
        {
            var shippings = new List<Shipping>();
            var groupReportsShippings = await repository.GetItemsAsync();
            foreach (var item in groupReportsShippings.Where(gr =>gr.GroupReport.Id==id).ToList())
            {
                shippings.Add(item.Shipping);
            }
            return shippings;
        }
    }
}
