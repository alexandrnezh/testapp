﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class ReportsUsersStore : IReportsUsersStore
    {
        private ReportsUsersRepository repository;
        public ReportsUsersStore()
        {
            repository = new ReportsUsersRepository();
        }

        public async Task<List<Report>> GetReportsByUserIdAsync(string id)
        {
            List<Report> reports = new List<Report>();
            var reportsUsers = await repository.GetItemsAsync();
            foreach (var item in reportsUsers.Where(r => r.User.Id == id).ToList())
            {
                reports.Add(item.Report);
            }
            return reports;
        }
    }
}
