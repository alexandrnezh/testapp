﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class GroupReportsPlansStore : IGroupReportsPlansStore
    {
        private GroupReportsPlansRepository repository;
        public GroupReportsPlansStore()
        {
            repository = new GroupReportsPlansRepository();
        }

        public async Task<List<GroupReportsPlans>> GetGroupReportsPlansAsync()
        {
            return await repository.GetItemsAsync();
        }

        public async Task<List<Plan>> GetPlansByReportIdAsync(int id)
        {
            var plans = new List<Plan>();
            var groupReportsPlans = await repository.GetItemsAsync();
            foreach (var item in groupReportsPlans.Where(gr => gr.GroupReport.Id == id).ToList())
            {
                plans.Add(item.Plan);
            }
            return plans;
        }
    }
}
