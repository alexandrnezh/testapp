﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces.Stores;
using TestApplication.Infrastructure.Data.Repositories;

namespace TestApplication.Infrastructure.Business.Stores
{
    public class GroupReportStore : IGroupReportStore
    {
        private GroupReportRepository repository;
        public GroupReportStore()
        {
            repository = new GroupReportRepository();
        }
        public async Task<GroupReport> GetGroupReportAsync(int id)
        {
            return await repository.GetItemAsync(id);
        }

        public async Task<List<GroupReport>> GetGroupReportsAsync()
        {
            return await repository.GetItemsAsync();
        }

    }
}
