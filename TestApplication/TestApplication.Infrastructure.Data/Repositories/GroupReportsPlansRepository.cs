﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class GroupReportsPlansRepository : IRepository<GroupReportsPlans>
    {
        private ApplicationDbContext db;
        public GroupReportsPlansRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(GroupReportsPlans item)
        {
            db.GroupReports.Attach(item.GroupReport);
            db.Plans.Attach(item.Plan);
            await db.GroupReportsPlans.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            GroupReportsPlans reportsPlans = await db.GroupReportsPlans.FindAsync(id);
            if (reportsPlans != null)
            {
                db.Remove(reportsPlans);
                await Save();
            }
        }

        public async Task<GroupReportsPlans> GetItemAsync(int id)
        {
            return await db.GroupReportsPlans.FindAsync(id);
        }

        public async Task<List<GroupReportsPlans>> GetItemsAsync()
        {
            var plans = await db.Plans.ToListAsync();
            var groupReports = await db.GroupReports.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            return await db.GroupReportsPlans.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(GroupReportsPlans item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
