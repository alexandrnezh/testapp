﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class GroupReportRepository : IRepository<GroupReport>
    {
        private ApplicationDbContext db;
        public GroupReportRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(GroupReport item)
        {
            db.Users.Attach(item.Creator);
            await db.GroupReports.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            GroupReport report = await db.GroupReports.FindAsync(id);
            if (report != null)
            {
                db.Remove(report);
                await Save();
            }
        }

        public async Task<GroupReport> GetItemAsync(int id)
        {
            var plans = await db.Plans.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            return await db.GroupReports.FindAsync(id);
        }

        public async Task<List<GroupReport>> GetItemsAsync()
        {
            var plans = await db.Plans.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            return await db.GroupReports.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(GroupReport item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
