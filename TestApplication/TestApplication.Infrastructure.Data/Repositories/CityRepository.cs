﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class CityRepository : IRepository<City>
    {
        private ApplicationDbContext db;
        public CityRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(City item)
        {
            await db.Cities.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            City post = await db.Cities.FindAsync(id);
            if (post != null)
            {
                db.Remove(post);
                await Save();
            }
        }

        public async Task<City> GetItemAsync(int id)
        {
            return await db.Cities.FindAsync(id);
        }

        public async Task<List<City>> GetItemsAsync()
        {
            return await db.Cities.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(City item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
