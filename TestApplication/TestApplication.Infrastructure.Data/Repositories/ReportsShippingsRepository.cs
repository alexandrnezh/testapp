﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class ReportsShippingsRepository : IRepository<ReportsShippings>
    {
        private ApplicationDbContext db;
        public ReportsShippingsRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(ReportsShippings item)
        {
             db.Reports.Attach(item.Report);
             db.Shippings.Attach(item.Shipping);
            await db.ReportsShippings.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            ReportsShippings reportsShippings = await db.ReportsShippings.FindAsync(id);
            if (reportsShippings != null)
            {
                db.Remove(reportsShippings);
                await Save();
            }
        }

        public async Task<ReportsShippings> GetItemAsync(int id)
        {
            return await db.ReportsShippings.FindAsync(id);
        }

        public async Task<List<ReportsShippings>> GetItemsAsync()
        {
            await db.Shippings.ToListAsync();
            await db.Reports.ToListAsync();
            return await db.ReportsShippings.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(ReportsShippings item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
