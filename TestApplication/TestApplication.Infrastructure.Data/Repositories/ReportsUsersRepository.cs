﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class ReportsUsersRepository : IRepository<ReportsUsers>
    {
        private ApplicationDbContext db;
        public ReportsUsersRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(ReportsUsers item)
        {
            db.Reports.Attach(item.Report);
            db.Users.Attach(item.User);
            await db.ReportsUsers.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            ReportsUsers reportsUsers = await db.ReportsUsers.FindAsync(id);
            if (reportsUsers != null)
            {
                db.Remove(reportsUsers);
                await Save();
            }
        }

        public async Task<ReportsUsers> GetItemAsync(int id)
        {
            return await db.ReportsUsers.FindAsync(id);
        }

        public async Task<List<ReportsUsers>> GetItemsAsync()
        {
            var plans = await db.Plans.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            var reports = await db.Reports.ToListAsync();
            var users = await db.Users.ToListAsync();
            return await db.ReportsUsers.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(ReportsUsers item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }

    }
}
