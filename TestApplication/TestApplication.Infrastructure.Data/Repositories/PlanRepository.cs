﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class PlanRepository : IRepository<Plan>
    {
        private ApplicationDbContext db;
        public PlanRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(Plan item)
        {
            await db.Plans.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            Plan plan = await db.Plans.FindAsync(id);
            if (plan != null)
            {
                db.Remove(plan);
                await Save();
            }
        }

        public async Task<Plan> GetItemAsync(int id)
        {
            return await db.Plans.FindAsync(id);
        }

        public async Task<List<Plan>> GetItemsAsync()
        {
            await db.Cities.ToListAsync();
            return await db.Plans.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(Plan item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
