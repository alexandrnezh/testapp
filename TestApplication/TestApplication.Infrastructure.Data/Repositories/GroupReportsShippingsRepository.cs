﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class GroupReportsShippingsRepository : IRepository<GroupReportsShippings>
    {
        private ApplicationDbContext db;
        public GroupReportsShippingsRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(GroupReportsShippings item)
        {
            db.GroupReports.Attach(item.GroupReport);
            db.Shippings.Attach(item.Shipping);
            await db.GroupReportsShippings.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            GroupReportsShippings reportsShippings = await db.GroupReportsShippings.FindAsync(id);
            if (reportsShippings != null)
            {
                db.Remove(reportsShippings);
                await Save();
            }
        }

        public async Task<GroupReportsShippings> GetItemAsync(int id)
        {
            return await db.GroupReportsShippings.FindAsync(id);
        }

        public async Task<List<GroupReportsShippings>> GetItemsAsync()
        {
            var shippings = await db.Shippings.ToListAsync();
            var groupReports = await db.GroupReports.ToListAsync();
            return await db.GroupReportsShippings.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(GroupReportsShippings item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
