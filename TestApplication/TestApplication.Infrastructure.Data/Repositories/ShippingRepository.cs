﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class ShippingRepository : IRepository<Shipping>
    {
        private ApplicationDbContext db;
        public ShippingRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(Shipping item)
        {
            
            await db.Shippings.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            Shipping shipping = await db.Shippings.FindAsync(id);
            if (shipping != null)
            {
                db.Remove(shipping);
                await Save();
            }
        }

        public async Task<Shipping> GetItemAsync(int id)
        {
            return await db.Shippings.FindAsync(id);
        }

        public async Task<List<Shipping>> GetItemsAsync()
        {
            return await db.Shippings.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(Shipping item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
