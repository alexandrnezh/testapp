﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class GroupReportsUsersRepository : IRepository<GroupReportsUsers>
    {
        private ApplicationDbContext db;
        public GroupReportsUsersRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(GroupReportsUsers item)
        {
            db.GroupReports.Attach(item.Report);
            db.Users.Attach(item.User);
            await db.GroupReportsUsers.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            GroupReportsUsers reportsUsers = await db.GroupReportsUsers.FindAsync(id);
            if (reportsUsers != null)
            {
                db.Remove(reportsUsers);
                await Save();
            }
        }

        public async Task<GroupReportsUsers> GetItemAsync(int id)
        {
            return await db.GroupReportsUsers.FindAsync(id);
        }

        public async Task<List<GroupReportsUsers>> GetItemsAsync()
        {
            var plans = await db.Plans.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            var groupReports = await db.GroupReports.ToListAsync();
            var users = await db.Users.ToListAsync();
            return await db.GroupReportsUsers.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(GroupReportsUsers item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }

    }
}
