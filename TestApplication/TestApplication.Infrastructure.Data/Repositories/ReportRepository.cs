﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;
using TestApplication.Domain.Interfaces;

namespace TestApplication.Infrastructure.Data.Repositories
{
    public class ReportRepository : IRepository<Report>
    {
        private ApplicationDbContext db;
        public ReportRepository()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var options = optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=aspnet-TestApplication-3F9D5FB6-C24E-4956-B570-1EA693C55F49;Trusted_Connection=True;MultipleActiveResultSets=true").Options;
            this.db = new ApplicationDbContext(options);
        }
        public async Task Create(Report item)
        {
            db.Users.Attach(item.Creator);
            db.Plans.Attach(item.Plan);
            await db.Reports.AddAsync(item);
            await Save();
        }

        public async Task Delete(int id)
        {
            Report report = await db.Reports.FindAsync(id);
            if (report != null)
            {
                db.Remove(report);
                await Save();
            }
        }

        public async Task<Report> GetItemAsync(int id)
        {
            var plans = await db.Plans.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            return await db.Reports.FindAsync(id);
        }

        public async Task<List<Report>> GetItemsAsync()
        {
            var plans = await db.Plans.ToListAsync();
            var cities = await db.Cities.ToListAsync();
            return await db.Reports.ToListAsync();
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }

        public async Task Update(Report item)
        {
            db.Entry(item).State = EntityState.Modified;
            await Save();
        }
    }
}
