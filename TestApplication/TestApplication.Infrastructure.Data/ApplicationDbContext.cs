﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Infrastructure.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Plan> Plans { get; set; }
        public DbSet<Shipping> Shippings { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<ReportsShippings> ReportsShippings { get; set; }
        public DbSet<GroupReport> GroupReports { get; set; }
        public DbSet<GroupReportsPlans> GroupReportsPlans { get; set; }
        public DbSet<GroupReportsShippings> GroupReportsShippings { get; set; }
        public DbSet<GroupReportsUsers>  GroupReportsUsers { get; set; }
        public DbSet<ReportsUsers> ReportsUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
