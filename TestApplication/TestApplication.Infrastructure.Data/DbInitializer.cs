﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Infrastructure.Data
{
    public static class DbInitializer

    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Cities.Any())
            {
                return;   // DB has been seeded
            }
            City city1 = new City() { Name = "Dnipro"};
            City city2 = new City() { Name = "Odesa"};
            City city3 = new City() { Name = "Kiyv" };
            City city4 = new City() { Name = "Lviv" };
            City city5 = new City() { Name = "Donetsk" };
            City city6 = new City() { Name = "Lutsk" };

            Shipping shipping1 = new Shipping() { ArrivalCityId = 1, DepartureCityId = 2, Date = DateTime.Now };
            Shipping shipping2 = new Shipping() { ArrivalCityId = 1, DepartureCityId = 2, Date = DateTime.Now.AddDays(-1) };
            Shipping shipping3 = new Shipping() { ArrivalCityId = 1, DepartureCityId = 2, Date = DateTime.Now.AddDays(-2) };
            Shipping shipping4 = new Shipping() { ArrivalCityId = 1, DepartureCityId = 2, Date = DateTime.Now.AddDays(-3) };
            Shipping shipping5 = new Shipping() { ArrivalCityId = 1, DepartureCityId = 2, Date = DateTime.Now.AddDays(-4) };
            Shipping shipping6 = new Shipping() { ArrivalCityId = 1, DepartureCityId = 2, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping7 = new Shipping() { ArrivalCityId = 3, DepartureCityId = 2, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping8 = new Shipping() { ArrivalCityId = 3, DepartureCityId = 2, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping9 = new Shipping() { ArrivalCityId = 3, DepartureCityId = 2, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping10 = new Shipping() { ArrivalCityId = 4, DepartureCityId = 5, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping11 = new Shipping() { ArrivalCityId = 4, DepartureCityId = 5, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping12 = new Shipping() { ArrivalCityId = 4, DepartureCityId = 5, Date = DateTime.Now.AddDays(-5) };
            Shipping shipping13 = new Shipping() { ArrivalCityId = 4, DepartureCityId = 5, Date = DateTime.Now.AddDays(-6) };
            Shipping shipping14 = new Shipping() { ArrivalCityId = 4, DepartureCityId = 5, Date = DateTime.Now.AddDays(-6) };
            Shipping shipping15 = new Shipping() { ArrivalCityId = 4, DepartureCityId = 5, Date = DateTime.Now.AddDays(-6) };
            Shipping shipping16 = new Shipping() { ArrivalCityId = 6, DepartureCityId = 5, Date = DateTime.Now.AddDays(-3) };
            Shipping shipping17 = new Shipping() { ArrivalCityId = 6, DepartureCityId = 5, Date = DateTime.Now.AddDays(-2) };
            Shipping shipping18 = new Shipping() { ArrivalCityId = 6, DepartureCityId = 5, Date = DateTime.Now.AddDays(-8) };
            Shipping shipping19 = new Shipping() { ArrivalCityId = 6, DepartureCityId = 5, Date = DateTime.Now.AddDays(-6) };
            Shipping shipping20 = new Shipping() { ArrivalCityId = 6, DepartureCityId = 5, Date = DateTime.Now.AddDays(-7) };
            Shipping shipping21 = new Shipping() { ArrivalCityId = 6, DepartureCityId = 5, Date = DateTime.Now.AddDays(-2) };

            Plan plan1 = new Plan() { ArrivalCity = city1, DepartureCity = city2, Month = 1, PlannedAmount = 100 };
            Plan plan2 = new Plan() { ArrivalCity = city3, DepartureCity = city2, Month = 1, PlannedAmount = 30 };
            Plan plan3 = new Plan() { ArrivalCity = city5, DepartureCity = city4, Month = 1, PlannedAmount = 50 };
            Plan plan4 = new Plan() { ArrivalCity = city6, DepartureCity = city4, Month = 1, PlannedAmount = 70 };


            context.Plans.AddRange(plan1,plan2,plan3,plan4);
            context.Cities.AddRange(new List<City>() { city1,city2,city3,city4,city5,city6 });
            context.Shippings.AddRange(new List<Shipping>() { shipping1, shipping2, shipping3, shipping4, shipping5, shipping6, shipping7,shipping8,shipping9,shipping10,shipping11,shipping12,shipping13,shipping14,shipping15,shipping16,shipping17,shipping18,shipping19,shipping20,shipping21 });
            context.SaveChanges();
        }
    }
}
