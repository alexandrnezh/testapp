﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestApplication.Migrations
{
    public partial class Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupReportsUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReportId = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupReportsUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupReportsUsers_GroupReports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "GroupReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupReportsUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportsUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReportId = table.Column<int>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportsUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReportsUsers_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportsUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupReportsUsers_ReportId",
                table: "GroupReportsUsers",
                column: "ReportId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupReportsUsers_UserId",
                table: "GroupReportsUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportsUsers_ReportId",
                table: "ReportsUsers",
                column: "ReportId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportsUsers_UserId",
                table: "ReportsUsers",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupReportsUsers");

            migrationBuilder.DropTable(
                name: "ReportsUsers");
        }
    }
}
