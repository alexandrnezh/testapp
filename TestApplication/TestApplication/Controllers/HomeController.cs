﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestApplication.Domain.Core.Models;
using TestApplication.Infrastructure.Business.Stores;
using TestApplication.Infrastructure.Data;
using TestApplication.Infrastructure.Data.Repositories;
using TestApplication.Models;

namespace TestApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<HomeController> _logger;
        private CityRepository _cityRepository;
        private PlanRepository _planRepository;
        private ShippingRepository _shippingRepository;
        private ReportRepository _reportRepository;
        private ReportsShippingsRepository _reportsShippingsRepository;
        private GroupReportsShippingsRepository _groupReportsShippingsRepository;
        private GroupReportsPlansRepository _groupReportsPlansRepository;
        private GroupReportRepository _groupReportRepository;
        private GroupReportsUsersRepository _groupReportsUsersRepository;
        private ReportsUsersRepository _reportsUsersRepository;
        private ApplicationDbContext _context;

        private CityStore _cityStore;
        private PlanStore _planStore;
        private ShippingStore _shippingStore;
        private ReportStore _reportStore;
        private GroupReportStore _groupReportStore;
        private GroupReportsPlansStore _groupReportsPlansStore;
        private GroupReportsShippingsStore _groupReportsShippingsStore;
        private ReportsShippingsStore _reportsShippingsStore;
        private GroupReportsUsersStore _groupReportsUsersStore;
        private ReportsUsersStore _reportsUsersStore;

        public HomeController(ILogger<HomeController> logger,
            CityRepository cityRepository,
            PlanRepository planRepository,
            ShippingRepository shippingRepository,
            ReportRepository reportRepository,
            PlanStore planStore,
            ShippingStore shippingStore,
            ReportStore reportStore,
            CityStore cityStore,
            GroupReportStore groupReportStore,
            GroupReportsPlansStore groupReportsPlansStore,
            GroupReportsShippingsStore groupReportsShippingsStore,
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            ReportsShippingsRepository reportsShippingsRepository,
            GroupReportsShippingsRepository groupReportsShippingsRepository,
            GroupReportsPlansRepository groupReportsPlansRepository,
            GroupReportRepository groupReportRepository,
            ReportsShippingsStore reportsShippingsStore,
            GroupReportsUsersRepository groupReportsUsersRepository,
            ReportsUsersRepository reportsUsersRepository,
            GroupReportsUsersStore groupReportsUsersStore,
            ReportsUsersStore reportsUsersStore,
            ApplicationDbContext context)
        {
            _context = context;
            _logger = logger;
            _cityRepository = cityRepository;
            _planRepository = planRepository;
            _shippingRepository = shippingRepository;
            _reportRepository = reportRepository;
            _userManager = userManager;
            _cityStore = cityStore;
            _planStore = planStore;
            _shippingStore = shippingStore;
            _reportStore = reportStore;
            _httpContextAccessor = httpContextAccessor;
            _reportsShippingsRepository = reportsShippingsRepository;
            _groupReportRepository = groupReportRepository;
            _groupReportsPlansRepository = groupReportsPlansRepository;
            _groupReportsShippingsRepository = groupReportsShippingsRepository;
            _groupReportStore = groupReportStore;
            _groupReportsShippingsStore = groupReportsShippingsStore;
            _groupReportsPlansStore = groupReportsPlansStore;
            _reportsShippingsStore = reportsShippingsStore;
            _groupReportsUsersRepository = groupReportsUsersRepository;
            _reportsUsersRepository = reportsUsersRepository;
            _groupReportsUsersStore = groupReportsUsersStore;
            _reportsUsersStore = reportsUsersStore;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {

            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var reports = await _reportsUsersStore.GetReportsByUserIdAsync(userId);
            var groupReports = await _groupReportsUsersStore.GetReportsByUserIdAsync(userId);
            var plans = await _planStore.GetPlansAsync();
            var groupReportsPlans = await _groupReportsPlansStore.GetGroupReportsPlansAsync();
            return View(new ReportsViewModel() { Reports = reports, GroupReports = groupReports, Plans = plans, GroupReportsPlans = groupReportsPlans });
        }
        [Authorize]
        public async Task<IActionResult> CreatePlanFactReport()
        {
            List<City> depCities = new List<City>();
            List<City> arrCities = new List<City>();
            List<Month> months = new List<Month>();
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (_context)
            {
                users = _context.Users.ToList();
            }

            var plans = await _planStore.GetPlansAsync();
            foreach (var item in plans.GroupBy(p => p.DepartureCity.Id).Select(p => p.FirstOrDefault()).ToList())
            {
                depCities.Add(item.DepartureCity);
            }
            foreach (var item in plans.GroupBy(p => p.ArrivalCity.Id).Select(p => p.FirstOrDefault()).ToList())
            {
                arrCities.Add(item.ArrivalCity);
            }
            foreach (var item in plans.GroupBy(p => p.Month).Select(p => p.FirstOrDefault()).ToList())
            {
                months.Add(new Month(item.Month));
            }
            return View(new CreatePlanFactViewModel() { DepartureCities = depCities, ArrivalCities = arrCities, Months = months, Users = users });
        }
        [Authorize]
        public async Task<IActionResult> CreateFactGroupReport()
        {
            List<City> cities = new List<City>();
            List<Month> months = new List<Month>();
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (_context)
            {
                users = _context.Users.ToList();
            }
            var plans = await _planStore.GetPlansAsync();
            foreach (var item in plans.GroupBy(p => p.DepartureCity.Id).Select(p => p.FirstOrDefault()).ToList())
            {
                cities.Add(item.DepartureCity);
            }
            foreach (var item in plans.GroupBy(p => p.Month).Select(p => p.FirstOrDefault()).ToList())
            {
                months.Add(new Month(item.Month));
            }
            return View(new CreateFactGroupViewModel() { DepartureCities = cities, Months = months, Users = users });
        }
        [Authorize]
        public async Task<IActionResult> ShowPlanFactReport(int id)
        {
            if (id != null)
            {
                var report = await _reportStore.GetReportAsync(id);
                var shippings = await _reportsShippingsStore.GetShippingsByReportIdAsync(id);
                return View(new PlanFactViewModel() { Shippings = shippings, Report = report });
            }
            return RedirectToAction("Index");
        }
        [Authorize]
        public async Task<IActionResult> ShowGroupFactReport(int id)
        {
            if (id != null)
            {
                var plans = await _groupReportsPlansStore.GetPlansByReportIdAsync(id);
                var shippings = await _groupReportsShippingsStore.GetShippingsByReportIdAsync(id);
                return View(new GroupFactReportViewModel() { Shippings = shippings, Plans = plans });
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreatePlanFactReport(int depCityId, int arrCityId, int month, IEnumerable<string> userIds)
        {
            var plan = await _planStore.GetPlanByMonthCitiesAsync(month, depCityId, arrCityId);
            var shippings = await _shippingStore.GetShippingsByMonthCitiesAsync(month, depCityId, arrCityId);
            if (shippings == null || plan == null)
            {
                return RedirectToAction("CreatePlanFactReport");
            }
            int factAmount = 0;
            foreach (var item in shippings)
            {
                factAmount++;
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            ApplicationUser currentUser = _userManager.FindByIdAsync(userId).Result;
            Report report = new Report() { Plan = plan, FactAmount = factAmount, Creator = currentUser };
            await _reportRepository.Create(report);
            var reports = await _reportStore.GetReportsAsync();
            Report createdReport = await _reportStore.GetReportAsync(reports.LastOrDefault().Id);
            foreach (var item in shippings)
            {
                ReportsShippings reportsShippings = new ReportsShippings() { Report = createdReport, Shipping = item };
                await _reportsShippingsRepository.Create(reportsShippings);
            }
            foreach (var item in userIds)
            {
                ApplicationUser user = _userManager.FindByIdAsync(item).Result;
                ReportsUsers reportsUsers = new ReportsUsers() { User = user, Report = createdReport };
                await _reportsUsersRepository.Create(reportsUsers);
            }
            await _reportsUsersRepository.Create(new ReportsUsers() { User = currentUser, Report = createdReport });
            return View("ShowPlanFactReport", new PlanFactViewModel() { Shippings = shippings, Report = createdReport });
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateFactGroupReport(IEnumerable<int> depCityIds, int month, IEnumerable<string> userIds)
        {
            List<Plan> plans = new List<Plan>();
            List<Shipping> shippings = new List<Shipping>();
            foreach (var item in depCityIds)
            {
                var curPlans = await _planStore.GetPlansByMonthDepartureCityAsync(month, item);
                shippings.AddRange(await _shippingStore.GetShippingsByMonthDepartureCityAsync(month, item));
                if (curPlans.Count != 0)
                {
                    plans.AddRange(curPlans);
                }
            }

            if (shippings.Count == 0 || plans.Count == 0)
            {
                return RedirectToAction("CreateFactGroupReport");
            }
            var userId = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            ApplicationUser currentUser = _userManager.FindByIdAsync(userId).Result;

            GroupReport report = new GroupReport() { Creator = currentUser };
            await _groupReportRepository.Create(report);
            var reports = await _groupReportStore.GetGroupReportsAsync();
            GroupReport createdReport = await _groupReportStore.GetGroupReportAsync(reports.LastOrDefault().Id);
            foreach (var item in shippings)
            {
                GroupReportsShippings reportsShippings = new GroupReportsShippings() { GroupReport = createdReport, Shipping = item };
                await _groupReportsShippingsRepository.Create(reportsShippings);
            }
            foreach (var item in plans)
            {
                GroupReportsPlans reportsPlans = new GroupReportsPlans() { GroupReport = createdReport, Plan = item };
                await _groupReportsPlansRepository.Create(reportsPlans);
            }
            foreach (var item in userIds)
            {
                ApplicationUser user = _userManager.FindByIdAsync(item).Result;
                GroupReportsUsers reportsUsers = new GroupReportsUsers() { User = user, Report = createdReport };
                await _groupReportsUsersRepository.Create(reportsUsers);
            }
            await _groupReportsUsersRepository.Create(new GroupReportsUsers() {User=currentUser, Report=createdReport });
            return View("ShowGroupFactReport", new GroupFactReportViewModel() { Shippings = shippings, Plans = plans });
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
