﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Models
{
    public class CreateFactGroupViewModel
    {
        public List<City> DepartureCities { get; set; }
        public List<Month> Months { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}
