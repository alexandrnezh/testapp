﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApplication.Models
{
    public class Month
    {
        public int Idx { get; set; }
        public string Name { get; set; }

        public Month(int id)
        {
            Idx = id;
            switch (Idx)
            {
                case 1: Name = "January"; break;
                case 2: Name = "February"; break;
                case 3: Name = "March"; break;
                case 4: Name = "April"; break;
                case 5: Name = "May"; break;
                case 6: Name = "May"; break;
                case 7: Name = "July"; break;
                case 8: Name = "August"; break;
                case 9: Name = "September"; break;
                case 10: Name = "October"; break;
                case 11: Name = "November"; break;
                case 12: Name = "December"; break;
            }
        }
    }
}
