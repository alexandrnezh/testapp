﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Models
{
    public class PlanFactViewModel
    {
        public Report Report { get; set; }
        public List<Shipping> Shippings { get; set; }
    }
}
