﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Models
{
    public class ReportsViewModel
    {
        public List<Report> Reports { get; set; }
        public List<GroupReport> GroupReports { get; set; }
        public List<GroupReportsPlans> GroupReportsPlans { get; set; }
        public List<Plan> Plans { get; set; }
    }
}
