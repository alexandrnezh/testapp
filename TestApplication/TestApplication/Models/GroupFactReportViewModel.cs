﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Models
{
    public class GroupFactReportViewModel
    {
        public List<Shipping> Shippings { get; set; }
        public List<Plan> Plans { get; set; }
    }
}
