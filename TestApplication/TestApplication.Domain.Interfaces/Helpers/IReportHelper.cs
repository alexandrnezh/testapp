﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Domain.Interfaces.Helpers
{
    public interface IReportHelper
    {
        Report CreateReport(Plan plan, List<Shipping> shippings);
    }
}
