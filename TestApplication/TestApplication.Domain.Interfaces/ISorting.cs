﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApplication.Domain.Interfaces
{
    public interface ISorting<T>
    {
        List<T> Sort(List<T> items);
    }
}
