﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Domain.Interfaces.Stores
{
    public interface IGroupReportsPlansStore
    {
        Task<List<Plan>> GetPlansByReportIdAsync(int id);
        Task<List<GroupReportsPlans>> GetGroupReportsPlansAsync();
    }
}
