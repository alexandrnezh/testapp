﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Domain.Interfaces.Stores
{
    public interface IPlanStore
    {
        Task<Plan> GetPlanByMonthCitiesAsync(int month, int depCityId, int arrCityId);
        Task<List<Plan>> GetPlansByMonthDepartureCityAsync(int month, int depCityId);
        Task<List<Plan>> GetPlansAsync();
        Task<Plan> GetPlanAsync(int id);
    }
}
