﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Domain.Interfaces.Stores
{
    public interface ICityStore
    {
        Task<List<City>> GetCitiesAsync();
        Task<City> GetCityAsync(int id);
    }
}
