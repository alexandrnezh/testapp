﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Domain.Interfaces.Stores
{
    public interface IShippingStore
    {
        Task<List<Shipping>> GetShippingsByMonthCitiesAsync(int month, int depCityId, int arrCityId);
        Task<List<Shipping>> GetShippingsByMonthDepartureCityAsync(int month, int depCityId);
        Task<List<Shipping>> GetShippingsAsync();
        Task<Shipping> GetShippingAsync(int id);
    }
}
