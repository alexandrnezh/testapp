﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Domain.Core.Models;

namespace TestApplication.Domain.Interfaces.Stores
{
    public interface IReportStore
    {
        Task<List<Report>> GetReportsAsync();
        Task<Report> GetReportAsync(int id);
    }
}
