﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestApplication.Domain.Interfaces
{
    public interface IRepository<T>
    {
        Task Create(T item);
        Task Delete(int id);
        Task Save();
        Task<T> GetItemAsync(int id);
        Task<List<T>> GetItemsAsync();
        Task Update(T item);
    }
}
